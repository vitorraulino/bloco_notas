package com.example.notas;

import android.content.Context;
import android.content.SharedPreferences;

public class Notas {
    SharedPreferences notas;
    SharedPreferences.Editor editor;
    Context ct;
    private static final String PREF_FILES = "Notas";
    public Notas(Context c){
        this.ct = c;
        notas = this.ct.getSharedPreferences(PREF_FILES,Context.MODE_PRIVATE);
    }
}
